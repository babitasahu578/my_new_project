import React, { PureComponent } from "react";

class Person1 extends PureComponent {
  state = {
    showDataList: [],
    details: {
      firstName: "Neel",
      lastName: "Roy",
      Id: "m1057879",
      emailId: "neelroy@mindtree.com",
      officeAddress: {
        streetAddress:
          "#150, EPIP 2nd phase, KIADB Export Promotion Industrial Area,Whitefield",
        city: "Bangalore",
        state: "Karnataka",
        country: "India",
        pinCode: " 560066",
      },
      Education: [
        {
          institute: "New Horizon College of Engineering",
          subject: "Financial Management",
          course: "MBA",
        },
      ],
    },
  };

  render() {
    console.log("this is purecomponent");
    return (
      <div>
        <button onClick={(e) => this.props.employeeDetailHandle(this.state)}>
          Pure Component{" "}
        </button>
      </div>
    );
  }
}

export default Person1;
