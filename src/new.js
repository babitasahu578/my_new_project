import React, { Component } from "react";
import "./App.css";
// import Person from "./Component/Person";
// import Person1 from "./Component/Person1";

class App extends Component {
  state = {
    showDataList: [],
    details: {
      firstName: "Neel",
      lastName: "Roy",
      Id: "m1057879",
      emailId: "neelroy@mindtree.com",
      officeAddress: {
        streetAddress:
          "#150, EPIP 2nd phase, KIADB Export Promotion Industrial Area,Whitefield",
        city: "Bangalore",
        state: "Karnataka",
        country: "India",
        pinCode: " 560066",
      },
      Education: [
        {
          institute: "New Horizon College of Engineering",
          subject: "Financial Management",
          course: "MBA",
        },
      ],
    },
  };

  objectHandle = (values) => {
    console.log(values, "person details");
    this.setState({
      firstName: values.details.firstName,
      lastName: values.details.lastName,
      Id: values.details.Id,
      emailId: values.details.emailId,
      streetAddress: values.details.officeAddress.streetAddress,
      city: values.details.officeAddress.city,
      state: values.details.officeAddress.state,
      country: values.details.officeAddress.country,
      pinCode: values.details.officeAddress.pinCode,
      institute: values.details.Education[0].institute,
      subject: values.details.Education[0].subject,
      course: values.details.Education[0].course,
    });
  };
  onDataChange = (event) => {
    const employeeDetails = {
      [event.target.name]: event.target.value,
    };
    this.setState(employeeDetails);
  };
  handleSubmit = () => {
    const updatedata = this.state;
    console.log(updatedata);
    this.props.handleUpdatedData(updatedata);
  };

  render() {
    console.log("rendering data");
    const {
      firstName,
      lastName,
      Id,
      emailId,
      streetAddress,
      city,
      state,
      country,
      pinCode,
      institute,
      subject,
      course,
    } = this.state;
    return (
      <div>
        <Person employeeDetailHandle={this.objectHandle} />
        <br />
        <Person1 employeeDetailHandle={this.objectHandle} />
        First Name:
        <label>
          <input
            type="text"
            name="firstName"
            value={firstName}
            onChange={this.onDataChange}
          />
          <br />
        </label>
        Last Name:
        <label>
          <input
            name="lastName"
            type="text"
            value={lastName}
            onChange={this.onDataChange}
          />

          <br />
        </label>
        Id:
        <label>
          <input
            type="text"
            value={Id}
            onChange={this.onDataChange}
            name="Id"
          />
          <br />
        </label>
        Email-Id:
        <label>
          <input
            type="email"
            value={emailId}
            onChange={this.onDataChange}
            name="emailId"
          />
          <br />
        </label>
        <label>
          Street:
          <input
            name="streetAddress"
            type="text"
            value={streetAddress}
            onChange={this.onDataChange}
          />
          <br />
        </label>
        City:
        <label>
          <input
            type="text"
            value={city}
            onChange={this.onDataChange}
            name="city"
          />
          <br />
        </label>
        State:
        <label>
          <input
            name="state"
            type="text"
            value={state}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        country:
        <label>
          <input
            name="country"
            type="text"
            value={country}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        Pincode:
        <label>
          <input
            name="pincode"
            type="number"
            value={pinCode}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        Institute:
        <label>
          <input
            name="institute"
            type="text"
            value={institute}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        Subject:
        <label>
          <input
            name="subject"
            type="text"
            value={subject}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        Course:
        <label>
          <input
            name="course"
            type="text"
            value={course}
            onChange={this.onDataChange}
          />
        </label>
        <br />
        <br />
        <button onClick={this.handleSubmit}>Update</button>
      </div>
    );
  }
}

export default App;
