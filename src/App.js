import React, { Component } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Header from "./Pages/Header";
import Scenario1 from "./Pages/Scenario1";
import Scenario2 from "./Pages/Scenario2";
import Scenario3 from "./Pages/Scenario3";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDataList: [],
      details: {
        firstName: "Neel",
        lastName: "Roy",
        Id: "m1057879",
        emailId: "neelroy@mindtree.com",
        officeAddress: {
          streetAddress:
            "#150, EPIP 2nd phase, KIADB Export Promotion Industrial Area,Whitefield",
          city: "Bangalore",
          state: "Karnataka",
          country: "India",
          pinCode: " 560066",
        },
        Education: [
          {
            institute: "New Horizon College of Engineering",
            subject: "Financial Management",
            course: "MBA",
          },
        ],
      },
    };
  }


  render() {
    console.log(this.state);
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/scenario1" component={Scenario1}></Route>
          <Route exact path="/scenario2" component={Scenario2}></Route>
          <Route exact path="/scenario3" component={Scenario3}></Route>
        </Switch>
      </div>
    );
  }
}

export default App;
