import React, { Component } from "react";

class Scenario1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDataList: [],
      details: {
        firstName: "Neel",
        lastName: "Roy",
        Id: "m1057879",
        emailId: "neelroy@mindtree.com",
        officeAddress: {
          streetAddress:
            "#150, EPIP 2nd phase, KIADB Export Promotion Industrial Area,Whitefield",
          city: "Bangalore",
          state: "Karnataka",
          country: "India",
          pinCode: " 560066",
        },
        Education: [
          {
            institute: "New Horizon College of Engineering",
            subject: "Financial Management",
            course: "MBA",
          },
        ],
      },
    };
  }

  render() {
    return <div>
        
    </div>;
  }
}

export default Scenario1;
