import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import { Link } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link to="/">Home</Link>
          </Typography>
          <Typography variant="h6" className={classes.title}>
            <Link to="/scenario1">Scenario1</Link>
          </Typography>
          <Typography variant="h6" className={classes.title}>
            <Link to="/scenario2">Scenario1</Link>
          </Typography>
          <Typography variant="h6" className={classes.title}>
            <Link to="/scenario3">Scenario1</Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
